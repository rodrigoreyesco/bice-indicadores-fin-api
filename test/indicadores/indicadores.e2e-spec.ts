import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';

describe('IndicadoresController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/indicadores/ultimos (GET)', async () => {
    return await request(app.getHttpServer())
      .get('/indicadores/ultimos')
      .expect(200)
      .field("cobre.key", "cobre")
      .field("dolar.key", "dolar")
  });

  it('/indicadores/historial/uf (GET)', async () => {
    return await request(app.getHttpServer())
      .get('/indicadores/historial/uf')
      .expect(200)
      .field("key", "uf")
      .field("name", "Unidad de fomento")
      .field("unit", "pesos")
  });

});
