import { Module, HttpModule, CacheModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { IndicadoresController } from './indicadores/indicadores.controller';
import { IndicadoresService } from './indicadores/indicadores.service';

@Module({
  imports: [ConfigModule.forRoot(), CacheModule.register(), HttpModule],
  controllers: [IndicadoresController],
  providers: [IndicadoresService],
})
export class AppModule { }
