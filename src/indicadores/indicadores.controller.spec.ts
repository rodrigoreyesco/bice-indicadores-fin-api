import { Test, TestingModule } from '@nestjs/testing';
import { IndicadoresController } from './indicadores.controller';
import { IndicadoresService } from './indicadores.service';
import { HttpModule, CacheModule, NotFoundException } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Observable } from 'rxjs';

describe('Indicadores Controller', () => {
  let controller: IndicadoresController;
  let service: IndicadoresService;
  const elementos: Array<string> = ['cobre', 'dolar', 'euro', 'ipc', 'ivp', 'oro', 'plata', 'uf', 'utm', 'yen'];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), CacheModule.register(), HttpModule],
      providers: [IndicadoresService],
      controllers: [IndicadoresController],
    }).compile();

    controller = module.get<IndicadoresController>(IndicadoresController);
    service = module.get<IndicadoresService>(IndicadoresService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('Obtener todos los Ultimos Indicadores', () => {

    it('La variable service debe estar definido', async () => {
      controller.getUltimosIndicadores()
      expect(service).toBeDefined();
    });

    it('Debe retornar instancia de Observable al invocar funcion', async () => {
      const value = controller.getUltimosIndicadores();
      expect(value).toBeInstanceOf(Observable);
    });

  });

  describe('Obtiene historial de indicadores con indicador no admitido', () => {
    it('indicador downj no debe ser encontrado', async () => {
      const param: string = "downj";
      try {
        controller.getHistorialIndicadoresPorElemento(param);
      } catch (e) {
        expect(e.response).toStrictEqual({
          statusCode: 404,
          message: 'Los valores posibles son: cobre,dolar,euro,ipc,ivp,oro,plata,uf,utm,yen',
          error: 'indicador para: downj no encontrado'
        })
      }
    });

    it('status code debe ser 404 al llamar con indicador UF', async () => {
      const param: string = "UF";
      try {
        controller.getHistorialIndicadoresPorElemento(param);
      } catch (e) {
        expect(e.status).toStrictEqual(404);
      }
    });

    it('debe retornar BadRequestException al invocar con parametro no definido', async () => {
      const param: string = null;
      try {
        controller.getHistorialIndicadoresPorElemento(param);
      } catch (e) {
        expect(e.response).toStrictEqual({
          statusCode: 400,
          message: 'Los valores posibles son: cobre,dolar,euro,ipc,ivp,oro,plata,uf,utm,yen',
          error: 'Debe especificar el indicador a consultar'
        })
      }
    });

    it('status code debe ser 400 al invocar con parametro no definido', async () => {
      const param: string = null;
      try {
        controller.getHistorialIndicadoresPorElemento(param);
      } catch (e) {
        expect(e.status).toStrictEqual(400);
      }
    });

    it('status code debe ser 404 al invocar con un paremetro numerico', async () => {
      const param: any = 100;
      try {
        controller.getHistorialIndicadoresPorElemento(param);
      } catch (e) {
        expect(e.status).toStrictEqual(404);
      }
    });

    it('Debe retornar instancia de Observable al invocar con un indicador aceptado', async () => {
      const param: string = "uf";
      const value = controller.getHistorialIndicadoresPorElemento(param);
      expect(value).toBeInstanceOf(Observable);
    });

  });


});
