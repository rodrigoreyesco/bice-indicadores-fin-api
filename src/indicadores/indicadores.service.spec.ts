import { Test, TestingModule } from '@nestjs/testing';
import { IndicadoresService } from './indicadores.service';
import { ConfigModule } from '@nestjs/config';
import { CacheModule, HttpModule } from '@nestjs/common';

describe('IndicadoresService', () => {
  let service: IndicadoresService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), CacheModule.register(), HttpModule],
      providers: [IndicadoresService],
    }).compile();

    service = module.get<IndicadoresService>(IndicadoresService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
