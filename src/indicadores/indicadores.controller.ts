import { Controller, Get, CacheInterceptor, UseInterceptors, CacheTTL, Param, NotFoundException, BadRequestException } from '@nestjs/common';
import { IndicadoresService } from './indicadores.service';

@Controller('indicadores')
@UseInterceptors(CacheInterceptor)
export class IndicadoresController {

    constructor(private indicadoresService: IndicadoresService) { }

    private elementos: Array<string> = ['cobre', 'dolar', 'euro', 'ipc', 'ivp', 'oro', 'plata', 'uf', 'utm', 'yen'];

    @CacheTTL(3600)
    @Get('ultimos')
    getUltimosIndicadores() {
        return this.indicadoresService.getUltimosIndicadores();
    }

    @CacheTTL(3600)
    @Get('historial/:elemento')
    getHistorialIndicadoresPorElemento(@Param('elemento') elemento: string) {
        if (!elemento) {
            throw new BadRequestException(`Los valores posibles son: ${this.elementos}`, 'Debe especificar el indicador a consultar');
        }
        if (!this.elementos.some(e => e === elemento)) {
            throw new NotFoundException(`Los valores posibles son: ${this.elementos}`, `indicador para: ${elemento} no encontrado`);
        }
        return this.indicadoresService.getHistorialIndicadoresPorElemento(elemento);
    }
}
