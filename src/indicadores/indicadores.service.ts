import { Injectable, HttpService } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class IndicadoresService {

    constructor(private http: HttpService) { }

    private apiUrl: string = process.env.INDECON_SERVICE_URL;

    /**
     * entrega los últimos valores de todos los elementos
     */
    getUltimosIndicadores(): Observable<any> {
        return this.http.get(`${this.apiUrl}/last`)
            .pipe(
                map(response => Object.keys(response.data).map(k => response.data[k]))
            );
    }

    /**
     * entrega un historial de los valores de un elemento particular
     * @param elemento 
     */
    getHistorialIndicadoresPorElemento(elemento: string): Observable<any> {
        return this.http.get(`${this.apiUrl}/values/${elemento}`)
            .pipe(
                map(response => response.data)
            );
    }

}
