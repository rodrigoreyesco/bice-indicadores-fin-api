# Indicadores Económicos - Desafío Bice Labs 

## Descripción
Servicio Backend para datos de indicaores economicos

## Instalación

```bash
$ npm install
```

## Ejecutar servicio en Development server

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Ejecutar Test unitarios y e2e test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Build en Google Cloud Run

```bash
gcloud builds submit --tag gcr.io/civil-listener-275722/indicadores-bice-api
```

## Autor

  rodrigo.reyesco@gmail.com
